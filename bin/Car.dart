// Создайте класс "Автомобиль" (Car), у которого есть свойства марка,
// модель и год выпуска. Реализуйте методы для получения  этих свойств.

class Car {
  String mark;
  String model;
  int year;

  Car(this.mark, this.model, this.year);

  String getMark() {
    return this.mark;
  }

  String getModel() {
    return this.model;
  }

  int getYear() {
    return this.year;
  }
}
