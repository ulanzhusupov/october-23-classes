// Создайте класс "Сотрудник" (Employee), у которого есть свойства имя,
// должность и зарплата. Реализуйте методы для получения этих свойств,
// а также метод для увеличения зарплаты на определенный процент.

class Employee {
  String name;
  String position;
  double salary;

  Employee({required this.name, required this.position, required this.salary});

  @override
  String toString() {
    
    return "Имя работника: $name\nДолжность: $position\nЗарплата: $salary";
  }

  void increaseZp(double percent) {
    salary += salary * (percent / 100);
  }
}