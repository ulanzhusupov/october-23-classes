/*
Создайте класс "Треугольник" (Triangle), у которого есть свойства длина сторон.
Реализуйте методы для получения этих свойств, а также метод для вычисления
площади треугольника по формуле Герона.
*/

class Triangle {
  double a;
  double b;
  double c;
  
  Triangle({required this.a, required this.b, required this.c});

  @override
  String toString() {
    
    return "Сторона а = $a\nСторона b = $b\nСторона c = $c";
  }

  double getPloshad() {
    double p = (a + b + c) / 2;
    print("Полупериметр = $p");

    return p * (p - a)*(p - b)*(p - c); 
  }
}