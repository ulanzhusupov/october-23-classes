// Создайте класс "Банковский счет" (BankAccount), у которого есть свойства номер счета,
// баланс и владелец. Реализуйте методы для получения этих свойств,
// а также методы для внесения и снятия денег.

class BankAccount {
  int requisite;
  double balance;
  String name;

  BankAccount({required this.requisite, required this.name, required this.balance});

  String showBalance() {
    return "Your balance: $balance";
  }

  void withdraw(int sum) {
    balance -= sum;
  }

  void topUpBalance(int sum) {
    balance += sum;
  }

  String showAccountInfo() {
    return "Your balance number: $requisite, your name is: $name";
  }

}