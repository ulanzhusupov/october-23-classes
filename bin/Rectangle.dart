// Создайте класс "Прямоугольник" (Rectangle), у которого есть свойства
// ширина и высота. Реализуйте методы для получения  этих свойств,
// а также методы для вычисления площади и периметра.

class Rectangle {
  double width;
  double height;
  Rectangle({required this.width, required this.height});

  double getPloshad() {
    return (width * height) / 2;
  }

  double getPerim() {
    return (width + height) * 2;
  }

  @override
  String toString() {
    return "Площадь равна ${getPloshad()}.\nПериметр равен: ${getPerim()}";
  }
}
