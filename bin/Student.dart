// Создайте класс "Студент" (Student), у которого есть свойства имя,
// возраст и средний балл. Реализуйте методы для получения  этих свойств.

class Student {
  String name;
  int age;
  List<int> points;

  Student({required this.name, required this.age, required this.points});

  String getName() {
    return this.name;
  }

  int getAge() {
    return this.age;
  }

  double getAveragePoint() {
    int sum = 0;
    for (int i = 0; i < points.length; i++) {
      sum += points[i];
    }

    return sum / 2;
  }
}
