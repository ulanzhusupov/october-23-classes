/*
Создайте класс "Круг" (Circle), у которого есть свойства радиус и цвет.
Реализуйте методы для получения этих свойств.

Создайте класс "Студент" (Student), у которого есть свойства имя,
возраст и средний балл. Реализуйте методы для получения  этих свойств.

Создайте класс "Автомобиль" (Car), у которого есть свойства марка,
модель и год выпуска. Реализуйте методы для получения  этих свойств.

Создайте класс "Книга" (Book), у которого есть свойства название,
автор и год издания. Реализуйте методы для получения этих свойств.

Создайте класс "Прямоугольник" (Rectangle), у которого есть свойства
ширина и высота. Реализуйте методы для получения  этих свойств,
а также методы для вычисления площади и периметра.

Создайте класс "Банковский счет" (BankAccount), у которого есть
свойства номер счета, баланс и владелец. Реализуйте методы для
получения этих свойств, а также методы для внесения и снятия денег.

Создайте класс "Товар" (Product), у которого есть свойства название,
цена и количество. Реализуйте методы для получения этих свойств,
а также метод для вычисления общей стоимости товара (умножение цены на количество).

Создайте класс "Сотрудник" (Employee), у которого есть свойства имя,
должность и зарплата. Реализуйте методы для получения этих свойств,
а также метод для увеличения зарплаты на определенный процент.

Создайте класс "Треугольник" (Triangle), у которого есть свойства
длина сторон. Реализуйте методы для получения этих свойств,
а также метод для вычисления площади треугольника по формуле Герона.

*/
import 'dart:math';

import 'Book.dart';
import 'Car.dart';
import 'Circle.dart';
import 'Student.dart';
import 'Rectangle.dart';
import 'BankAccount.dart';
import 'Employee.dart';
import 'Product.dart';
import 'Triangle.dart';

void main(List<String> arguments) {
  // Circle circle = Circle(32, "yellow");
  // print(circle.getRad());
  // print(circle.getColor());

  // Student student =
  //     Student(name: "Ulan", age: 22, points: [60, 50, 40, 80, 90]);

  // print(student.getAveragePoint());

  // Car car = Car("Toyota", "Moon", 2004);
  // print(car.getMark());

  // Book book = Book(
  //     title: "Когда падали горы",
  //     author: "Чынгыз Айтматов",
  //     productionYear: 2006);
  // print(book);

  // Rectangle rectangle = Rectangle(width: 255, height: 325);
  // print(rectangle);

  // BankAccount bankAccount = BankAccount(requisite: 1234567, name: "Mike Tyson", balance: 1000);
  // print(bankAccount.showAccountInfo());
  // print(bankAccount.showBalance());
  // bankAccount.topUpBalance(250000);
  // print(bankAccount.showBalance());
  // bankAccount.withdraw(50000);
  // print(bankAccount.showBalance());

  // Product product = Product(name: "Sheker", price: 88.9, count: 10);
  // print(product.getFullPrice(5));

  // Employee employee = Employee(name: "Ulan", position: "Flutter dev", salary: 70000);
  // print(employee);
  // employee.increaseZp(20);
  // print(employee);

  // Triangle triangle = Triangle(a: 6, b: 8, c: 10);
  // print(triangle);
  // print(triangle.getPloshad());
}
