// Создайте класс "Книга" (Book), у которого есть свойства название,
// автор и год издания. Реализуйте методы для получения этих свойств.

class Book {
  String title;
  String author;
  int productionYear;

  Book(
      {required this.title,
      required this.author,
      required this.productionYear});

  @override
  String toString() {
    // TODO: implement toString
    return "Название: ${this.title}, Автор: ${this.author}, Год выпуска: ${this.productionYear}";
  }
}
