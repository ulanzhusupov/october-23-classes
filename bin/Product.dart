// Создайте класс "Товар" (Product), у которого есть свойства название, цена и количество.
// Реализуйте методы для получения этих свойств, а также метод для вычисления
// общей стоимости товара (умножение цены на количество).

class Product {
  String name;
  double price;
  int count;

  Product({required this.name, required this.price, required this.count});

  @override
  String toString() {
    
    return "Название: $name, цена: $price, количество: $count";
  }

  double getFullPrice(int cnt) {
    return price * cnt;
  }



}