// Создайте класс "Круг" (Circle), у которого есть свойства радиус и цвет.
// Реализуйте методы для получения этих свойств.

class Circle {
  double rad;
  String color;

  Circle(this.rad, this.color);

  double getRad() {
    return this.rad;
  }

  String getColor() {
    return this.color;
  }
}
